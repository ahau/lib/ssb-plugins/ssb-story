# ssb-story

An `ssb-server` plugin for creating, reading and updating stories in `scuttlebutt`

## Modules

* ssb-story

---

## Story

A story is an archive entry that is stored using ssb.

```js
   A   (the root message)
   |
   B   (an edit after A)
  / \
 C   D (two concurrent edits after B)
```

Because there might be multiple offline edits to a story which didn't know bout one-another, it's possible for divergence to happen:
`C` and `D` wont know about each other.

A story is an Object which maps the key of each latest edit to the state it perceives the story to be in:

```js
// story for the example above
{
  key: MessageId, // storyId supplied, the root message of the artefact
  states: [
    { head: C, state: stateC },
    { head: D, state: stateD },
  ]
}
```

and the `state` is an Object which looks like the following:

```javascript
{
  type: String,
  title: String,
  description: String,
  timeInterval: EdtfIntervalString,
  submissionDate: EdtfDateString,
  location: String,
  locationDescription: String,

  // ADVANCED OPTIONS
  creator: String,
  contributorNotes: String,

  // ADDITIONAL METADATA
  format: String, // specific physical or digital form of the record, e.g. physical format (e.g. pamphlet, glass slide, open reel), duration or extent (e.g. 90 Minutes, 20 Pages), dimensions, digital file type (e.g. PDF, JPG, MP3), etc
  identifier: String, // unique reference to the record, e.g. call numbers or accession numbers
  language: String,
  source: String,
  transcription: String
  permission: String // a magic string with the value of either 'view', 'edit', or 'submit' which will be used on the frontend to handle member permissions

  authors: {
    [FeedId]: [
      { start: Integer, end: Integer }
    ]
  }
  recps: [FeedId]
  tombstone: Tombstone
}
```

## Collection

Collections are used to group related stories together into one place that is stored using ssb.

```javascript
{
  type: String,
  name: String,
  description: String,
  submissionDate: EdtfDateString,
  recordCount: Int,

  authors: {
    [FeedId]: [
      { start: Integer, end: Integer }
    ]
  }
  tombstone: Tombstone
  recps: [FeedId]
}
```

## API

### `server.story.create(type, details, cb)`

Creates a new story record and describes details you'd like to set on it

* `type` *String* - describes the type of story
* `details` *Object* - allows you to specifiy additional fields. See example below
* `cb` - *Function* - a callback with signature (err, storyId)

The expected form of `details`:

```js
{
  title: String,
  description: String,
  timeInterval: EdtfIntervalString,
  submissionDate: EdtfDateString,
  location: String,
  locationDescription: String,

  // ADVANCED OPTIONS
  creator: ProfileId,
  contributionNotes: String,

  // ADDITIONAL METADATA
  format: String,
  identifier: String,
  language: String,
  source: String,
  transcription: String,
  permission: String

  authors: {
    add: [FeedId, ALL_AUTHORS],
    remove: [FeedId, ALL_AUTHORS] // NOTE: not available on create
  }

  recps: [FeedId, ...]
}
```

* `EdtfIntervalString` - see [edtf module](https://www.npmjs.com/package/edtf) and [library of congress spec](http://www.loc.gov/standards/datetime/)
* `EdtfDateString` - see [edtf module](https://www.npmjs.com/package/edtf) and [library of congress spec](http://www.loc.gov/standards/datetime/)
* `recps` is a special scuttlebutt property, short for "recipients". Adding this means the artefact will automatically be encrypted so only the `FeedId` listed in the `recps` Array will be able to read this artefact.
* `authors` contains two arrays (add, remove) for adding and removing authors to the set. Authors are of the form `feedId` or `*`:
  * `FeedId` - updates from this author will be valid
  * `*` - updates by all authors will be valid
  * NOTE: authors are valid until they are removed from the set. When this feedId, it overrides all authors until it is removed
  * Any updates that arent from a valid author are classed as invalid and wont be returned when using the get method

---

### `server.story.get(storyId, cb)`

Gets a story record by its `storyId`

* `storyId` *String* - the cypherlink for the story (the `msgId` of the root message for the story)
* `cb` *Function* - a callback with signature `(err, story)`

All fields will be returned, but if no value has been set/added for that field, the value will be `null`

---

### `server.story.update(storyId, details, cb)`

Updates a story record by its `storyId`

* `storyId` *String* - the cypherlink for the story (the `msgId` of the root message for the story)
* `details` *Object* - same as in `story.create` except recps is set for you based on what the first message was and tombstone can be set. See below.
* cb Function - a callback with signature (err)

```js
tombstone: Tombstone,
```

type `Tombstone` is either `null` OR an Object of shape:

  ```js
  {
    date: UnixTime,  // an Integer indicating microseconds from 1st Jan 1970, can be negative!
    reason: String   // (optional)
  }
  ```

---

### `server.story.collection.create(type, details, cb)`

Creates a new collection record and describes details you'd like to set on it

* `type` *String* - describes the type of collection
* `details` *Object* - allows you to specifiy additional fields. See example below
* `cb` - *Function* - a callback with signature (err, collectionId)

The expected form of `details`:

```js
{
  name: String,
  description: String,
  image: Image,
  submissionDate: EdtfDateString,
  recordCount: Int,

  authors: {
    add: [FeedId, ALL_AUTHORS],
    remove: [FeedId, ALL_AUTHORS] // NOTE: not available on create
  }

  recps: [FeedId, ...]
}
```

---

### `server.story.collection.get(collectionId, cb)`

Gets a collection record by its `collectionId`

* `collectionId` *String* - the cypherlink for the collection (the `msgId` of the root message for the collection)
* `cb` *Function* - a callback with signature `(err, collection)`

All fields will be returned, but if no value has been set/added for that field, the value will be `null`

---

### `server.story.collection.update(collectionId, details, cb)`

Updates a collection record by its `collectionId`

* `collectionId` *String* - the cypherlink for the collection (the `msgId` of the root message for the collection)
* `details` *Object* - same as in `story.collection.create` except recps is set for you based on what the first message was and tombstone can be set.
* cb Function - a callback with signature (err)

---
